export const getToken = (state) => state.authentication_token
export const getUsers = (state) => state.users
export const getDepartments = (state) => state.departments
export const getAdmins = (state) => state.admins
export const getJobPositions = (state) => state.job_positions
