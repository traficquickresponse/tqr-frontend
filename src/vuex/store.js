import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash';

// Make vue aware of Vuex
Vue.use(Vuex)

// Create an object to hold the initial state when
// the app starts up
const SAData = JSON.parse(localStorage.getItem('SAData'));
const state = {
  authentication_token: (SAData) ? SAData.authentication_token : null,
  users: [],
  admins: [],
  departments: [],
  job_positions: []
}

// Create an object storing various mutations. We will write the mutation
// TODO: move this to another file, this will be getting messy fast
const mutations = {
  ADD_USERS (state, users) {
    users.map((user) => {
      user.is_admin = false;
      if(!_.find(state.users, {id: user.id})) state.users.push(user);
    });
  },
  SET_USER_ACTIVE (state, user) {
    let param;
    let tobeEdited;
    if(user.is_admin){
      tobeEdited = _.findIndex(state.admins, { id: user.id });
      param = 'admins';
    } else {
      tobeEdited = _.findIndex(state.users, { id: user.id });
      param = 'users';
    }
    state[param][tobeEdited] = Object.assign(state[param][tobeEdited], {is_non_active: false});
  },
  SET_USER_NON_ACTIVE (state, user) {
    let param;
    let tobeEdited;
    if(user.is_admin){
      tobeEdited = _.findIndex(state.admins, { id: user.id });
      param = 'admins';
    } else {
      tobeEdited = _.findIndex(state.users, { id: user.id });
      param = 'users';
    }
    state[param][tobeEdited] = Object.assign(state[param][tobeEdited], {is_non_active: true});
  },
  EDIT_USER (state, user) {
    let param;
    let tobeEdited;
    if(user.is_admin){
      tobeEdited = _.findIndex(state.admins, { id: user.id });
      param = 'admins';
    } else {
      tobeEdited = _.findIndex(state.users, { id: user.id });
      param = 'users';
    }
    state[param][tobeEdited] = Object.assign(state[param][tobeEdited], user);
  },
  ADD_ADMINS (state, admins) {
    admins.map((admin) => {
      admin.is_admin = true;
      if(!_.find(state.admins, {id: admin.id})) state.admins.push(admin);
    })
  },
  SET_TOKEN (state, token) {
    localStorage.setItem('SAData', `{"authentication_token": "${token}"}`);
    state.authentication_token = token;
  },
  SET_ADMIN (state, id) {
    const user    = _.find(state.users, {id: id});
    user.is_admin = true;
    state.admins.push(user);
    const toBeRemoved = _.findIndex(state.users, {id: id});
    state.users.splice(toBeRemoved, 1);
  },
  REMOVE_ADMIN (state, id) {
    const admin    = _.find(state.admins, {id: id});
    admin.is_admin = false;
    state.users.push(admin);
    const toBeRemoved = _.findIndex(state.admins, {id: id});
    state.admins.splice(toBeRemoved, 1);
  },
  ADD_DEPARTMENTS (state, departments) {
    departments.map((department) => {
      if(!_.find(state.departments, {id: department.id})) state.departments.push(department)
    })
  },
  EDIT_DEPARTMENT (state, department) {
    let tobeEdited;
    tobeEdited = _.findIndex(state.departments, { id: department.id });
    state.departments[tobeEdited] = Object.assign(state.departments[tobeEdited], department);
  },
  DELETE_DEPARTMENT (state, department) {
    let toBeDeleted = _.findIndex(state.departments, {id: department.id});
    state.departments.splice(toBeDeleted, 1);
  },
  ADD_JOB_POSITIONS (state, job_positions) {
    job_positions.map((job_position) => {
      if(!_.find(state.job_positions, {id: job_position.id})) state.job_positions.push(job_position)
    })
  },
  DELETE_JOB_POSITION (state, id) {
    const toBeDeleted = _.findIndex(state.job_positions, {id: id});
    state.job_positions.splice(toBeDeleted, 1);
  },
  UPDATE_JOB_POSITION (state, position) {
    const jobposition = _.find(state.job_positions, {id: position.id});
    jobposition.title = position.title;
  }
}

// Combine the initial state and the mutations to create a Vuex store.
// This store can be linked to our app.
export default new Vuex.Store({
  state,
  mutations
})
