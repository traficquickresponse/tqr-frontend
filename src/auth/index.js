import {router} from '../router.js'
import load from './../services/routes'

const API_URL = 'http://188.166.227.140:8686'
const LOGIN_URL = API_URL + '/admins/session/sign_in'
const SIGNUP_URL = API_URL + '/admins/session/sign_up'
const OPEN_CASES = API_URL + '/open_cases'

export default {

  user: {
    authenticated: false
  },

  logout() {
    localStorage.removeItem('lsData')
    this.user.authenticated = false
  },

  checkAuth() {
    var jwt = localStorage.getItem('lsData')
    if(jwt) {
      this.user.authenticated = true
    }
    else {
      this.user.authenticated = false
    }
  },


  getAuthHeader() {
    return {
      'Authorization': localStorage.getItem('lsData')
    }
  }
}
