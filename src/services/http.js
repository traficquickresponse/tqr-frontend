import axios from 'axios'

var conf = require('./config');

var http = axios.create({
    headers: {'Authorization': conf.SESSION}
});

module.exports = {
    GET   : function (observer, url, params) {
        http.get(url, {
            params: params
        })
            .then(function (response) {
                // console.log(response)
                observer.onNext(response.data.data);
            })
            .catch(function (error) {
                observer.onError(error);
            });
    },
    POST  : function (observer, url, params) {
        http.post(url,params)
            .then(function (response) {
                // console.log(response)
                observer.onNext(response.data.data);
            })
            .catch(function (error) {
                observer.onError(error);
            });
    },
    PUT   : function (observer, url, params) {
        // console.log(params)
        http.put(url, params)
            .then(function (response) {
                // console.log(response)
                observer.onNext(response.data.data);
            })
            .catch(function (error) {
                observer.onError(error);
            });
    },
    DELETE: function (observer, url, params) {
        http.delete(url, {
            params: params
        })
            .then(function (response) {
                // console.log(response)
                observer.onNext(response.data.data);
            })
            .catch(function (error) {
                observer.onError(error);
            });
    },
};
