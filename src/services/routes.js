var SESSION             = require('./requests/session');

module.exports = {
    postAdminSessionSignIn  : SESSION.post_AdminSessionSignIn,
    postAdminSessionSignUp  : SESSION.post_AdminSessionSignUp,
    postAdminSessionCases  : SESSION.post_AdminSessionCases,
    getAdminSessionCases  : SESSION.get_AdminSessionCases,
    getAdminSessionUsers  : SESSION.get_AdminSessionUsers,
    getAdminSessionAdminData  : SESSION.get_AdminSessionAdminData,
};
