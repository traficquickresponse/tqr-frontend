import Rx from 'rx'
var conf       = require('./../config');
var http       = require('./../http');
// https://www.npmjs.com/package/request-promise
module.exports = {
    post_AdminSessionSignIn  : function (params) {
         var source = Rx.Observable.create(function (observer) {
            http.POST(observer, conf.API_URL + "/admins/session/sign_in", params)
            return function () {console.log('disposed')}
        });
        return source
    },
    post_AdminSessionSignUp  : function (params) {
         var source = Rx.Observable.create(function (observer) {
            http.POST(observer, conf.API_URL + "/admins/session/sign_up", params)
            return function () {console.log('disposed')}
        });
        return source
    },
    post_AdminSessionCases  : function (params) {
         var source = Rx.Observable.create(function (observer) {
            http.POST(observer, conf.API_URL + "/open_cases", params)
            return function () {console.log('disposed')}
        });
        return source
    },
    get_AdminSessionCases  : function (params) {
         var source = Rx.Observable.create(function (observer) {
            http.GET(observer, conf.API_URL + "/open_cases", params)
            return function () {console.log('disposed')}
        });
        return source
    },
    get_AdminSessionUsers  : function (params) {
         var source = Rx.Observable.create(function (observer) {
            http.GET(observer, conf.API_URL + "/users", params)
            return function () {console.log('disposed')}
        });
        return source
    },
    get_AdminSessionAdminData  : function (params) {
         var source = Rx.Observable.create(function (observer) {
            http.GET(observer, conf.API_URL + "/admins", params)
            return function () {console.log('disposed')}
        });
        return source
    }
};
