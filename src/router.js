import Vue from 'vue'
import VueRouter from 'vue-router'

import Menu from './components/Menu.vue'
import Login from './components/login/Login.vue'
import Action from './components/action/Action.vue'
import ActionKecelakaan from './components/action/ActionKecelakaan.vue'
import ActionKemacetan from './components/action/ActionKemacetan.vue'
import ActionPlb from './components/action/ActionPlb.vue'
import ActionApelpers from './components/action/ActionApelpers.vue'
import PengPersNew from './components/pengpers/PengPersNew.vue'
import ActionNew from './components/action/ActionNew.vue'

// Super Admin
import AdminLogin from './components/superadmin/AdminLogin.vue'
import SuperAdmin from './components/superadmin/SuperAdmin.vue'
import Users from './components/superadmin/Users.vue'
import Departments from './components/superadmin/Departments.vue'
import JobPositions from './components/superadmin/JobPositions.vue'

Vue.use(VueRouter)

export var router = new VueRouter()

router.map({
    '/admin': {
      component: SuperAdmin,
      subRoutes: {
        '/': {
          component: Users
        },
        '/departments': {
          component: Departments
        },
        '/jobpositions': {
          component: JobPositions
        }
      }
    },
    '/admin-login': {
      component: AdminLogin
    },
    '/login'   : {
        component: Login
    },
    '/action': {
      component: ActionNew
    },
    // '/action'  : {
    //     component: Menu,
    //     subRoutes: {
    //         '/'          : {
    //             component: Action
    //         },
    //         '/kecelakaan': {
    //             component: ActionKecelakaan
    //         },
    //         '/kemacetan' : {
    //             component: ActionKemacetan
    //         },
    //         '/plb'       : {
    //             component: ActionPlb
    //         },
    //         '/apelpers'  : {
    //             component: ActionApelpers
    //         }
    //     }
    // },
    '/pengpers': {
        component: PengPersNew
    }
})

// debugger;
router.redirect({
    '*': '/login'
})
