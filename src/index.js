import Vue from 'vue'
import App from './components/App.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import _ from 'lodash'
Vue.use(VueResource)
import {router} from './router'
import auth from './auth'

Vue.http.headers.common['Authorization'] = localStorage.getItem('lsData');

// Check the user's auth status when the app starts
auth.checkAuth()

router.start(App, '#app')
